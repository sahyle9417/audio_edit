import os
import argparse
import urllib.parse
import urllib.request
from io import BytesIO
import subprocess
from tqdm import tqdm
import glob
import wave
import math
from pydub import AudioSegment

server_dict = {'jiin':'192.168.0.47',
               'hwang':'192.168.0.47',
               'son':'192.168.0.47',
               'yuinna':'192.168.0.47',
               'mbible':'192.168.0.47',
               'seoul20m01':'192.168.0.47',
               'seoul20w01':'192.168.0.47',
               'seoul20w02':'192.168.0.47',
               'seoul20w03':'192.168.0.47',
               'seoul20w04':'192.168.0.47',
               'seoul20w05':'192.168.0.47',
               }

port_dict = {'jiin':9002,
             'hwang':9002,
             'son':9002,
             'yuinna':9002,
             'mbible':9002,
             'seoul20m01':9000,
             'seoul20w01':9000,
             'seoul20w02':9000,
             'seoul20w03':9000,
             'seoul20w04':9000,
             'seoul20w05':9000
             }

yenim_dict = {'jiin':'jiin',
             'hwang':'hwang',
             'son':'son',
             'yuinna':'yuinna',
             'mbible':'mbible',
             'seoul20m01':'seoul20m02',
             'seoul20w01':'seoul20w01',
             'seoul20w02':'seoul20w02',
             'seoul20w03':'seoul20w03',
             'seoul20w04':'seoul20w04',
             'seoul20w05':'seoul20w05'
             }


def read_csv(input_csv):
    contents = []
    with open(input_csv, encoding='utf-8') as f:
        for line in f:
            single_line = line.strip().split('|')
            contents.append(single_line)
    return contents


def request_wav(speaker, text):
    encText = urllib.parse.quote(text)
    url = "http://{}:{}/wav2wavapi/".format(server_dict[speaker], port_dict[speaker])
    url += "yenim?yenim={}&malmal={}".format(yenim_dict[speaker], encText)
    request = urllib.request.Request(url)
    try:
        response = urllib.request.urlopen(request)
        rescode = response.getcode()
        if (rescode == 200):
            response_body = response.read()
            audio_file = BytesIO(response_body)
            return audio_file
        else:
            print('Error Code: %d', rescode)
    except Exception as ex:
            print('While accessing {}, call exception: {}'.format(url, ex))
    return None

def get_sample_rate(bgm_path):
    bgm_filename, bgm_ext = os.path.splitext(bgm_path)
    if bgm_ext == '.wav':
        with wave.open(bgm_path, "rb") as bgm_wavfile:
            return bgm_wavfile.getframerate()
    elif bgm_ext == '.mp3':
        bgm_mp3file = AudioSegment.from_mp3(bgm_path)
        return bgm_mp3file.frame_rate

def csv_to_wav(args):
    title = os.path.basename(args.input_csv)[:-4]
    src_path = os.path.join(args.output_dir, "src")
    pad_length = float(args.pad_length)/2
    bgm_volume = float(args.bgm_volume)
    contents = read_csv(args.input_csv)
    print("Generating audio files...")
    for i in tqdm(range(len(contents))):
        speaker = contents[i][0]
        text = contents[i][1]
        src_file = request_wav(speaker, text)
        src_filename = title+format(i+1,'05')+'.wav'
        with open(os.path.join(src_path, src_filename), 'wb') as out:
            out.write(src_file.read())
    print("Adding padding to audio files...")
    for src_filepath in glob.iglob(os.path.join(src_path, '*.wav')):
        src_temp = os.path.join(src_path, 'src_temp.wav')
        subprocess.run(["sox", src_filepath, src_temp, "pad", str(pad_length), str(pad_length)])
        subprocess.run(["mv", src_temp, src_filepath])

    print("Merge audio files...")
    full_path = os.path.join(args.output_dir, title+'.wav')
    subprocess.run(["sox", os.path.join(src_path, '*.wav'), full_path])

    if args.background_music is not None:
        print("Adding BGM...")
        bgm_path = args.background_music
        bgm_sample_rate = get_sample_rate(bgm_path)
        full_sample_rate = get_sample_rate(full_path)
        if bgm_sample_rate != full_sample_rate:
            bgm_filename, bgm_ext = os.path.splitext(bgm_path)
            modified_bgm_path = bgm_filename + "_sr" + str(full_sample_rate) + bgm_ext
            subprocess.run(["sox", bgm_path, "-r", str(full_sample_rate), modified_bgm_path])
            bgm_path = modified_bgm_path

        if bgm_volume is not None and bgm_volume != 1:
            bgm_filename, bgm_ext = os.path.splitext(bgm_path)
            modified_bgm_path = bgm_filename + "_vol" + str(bgm_volume) + bgm_ext
            subprocess.run(["sox", "-v", str(bgm_volume), bgm_path, modified_bgm_path])
            bgm_path = modified_bgm_path

        bgm_channel = int(subprocess.check_output(["soxi", "-c", bgm_path]))
        if bgm_channel != 1:
            bgm_filename, bgm_ext = os.path.splitext(bgm_path)
            modified_bgm_path = bgm_filename + "_c" + str(1) + bgm_ext
            subprocess.run(["sox", bgm_path, "-c", str(1), modified_bgm_path])
            bgm_path = modified_bgm_path

        full_with_bgm_filename, full_ext = os.path.splitext(full_path)
        full_with_bgm_path = full_with_bgm_filename + "_bgm" + full_ext

        full_length = int(subprocess.check_output(["soxi", "-s", full_path]))
        bgm_length = int(subprocess.check_output(["soxi", "-s", bgm_path]))
        if full_length > bgm_length:
            repeat = math.ceil(full_length/bgm_length)-1
            bgm_temp_path = os.path.join(os.path.dirname(bgm_path), "bgm_temp.wav")
            full_temp_path = os.path.join(os.path.dirname(full_path), "full_temp.wav")
            subprocess.run(["sox", bgm_path, bgm_temp_path, "repeat", str(int(repeat))]) # repeat bgm to match length
            subprocess.run(["sox", "-m", full_path, bgm_temp_path, full_temp_path])      # mix with full wav
            subprocess.run(["sox", full_temp_path, full_with_bgm_path, "trim", str(0), str(full_length / 24000)])  # trim
            subprocess.run(["rm", bgm_temp_path, full_temp_path])
        elif full_length < bgm_length:
            bgm_temp_path = os.path.join(os.path.dirname(bgm_path), "bgm_temp.wav")
            subprocess.run(["sox", bgm_path, bgm_temp_path, "trim", str(0), str(full_length/24000)])
            subprocess.run(["sox", "-m", full_path, bgm_temp_path, full_with_bgm_path])
            subprocess.run(["rm", bgm_temp_path])
        else:
            subprocess.run(["sox", "-m", full_path, bgm_path, full_with_bgm_path])

        full_with_bgm_filename, _ = os.path.splitext(full_with_bgm_path)
        full_with_bgm_mp3_path = full_with_bgm_filename + ".mp3"
        subprocess.run(["ffmpeg", "-i", full_with_bgm_path, "-vn", "-ac", "1", "-ab", "320k", "-f", "mp3", full_with_bgm_mp3_path])

    full_filename, _ = os.path.splitext(full_path)
    full_mp3_path = full_filename + ".mp3"
    subprocess.run(["ffmpeg", "-i", full_path, "-vn", "-ac", "1", "-ab", "320k", "-f", "mp3", full_mp3_path])

    if args.background_music is not None:
        print("Finish! Checkout {} and {}".format(full_mp3_path, full_with_bgm_mp3_path))
    else:
        print("Finish! Checkout %s" % full_mp3_path)



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-in', '--input_csv', default='./input.csv')
    parser.add_argument('-out', '--output_dir', default='./wav')
    parser.add_argument('-pad', '--pad_length', default='0.5')
    parser.add_argument('-bgm', '--background_music', default=None)
    parser.add_argument('-bgmvol', '--bgm_volume', default='1')
    args = parser.parse_args()
    os.makedirs(args.output_dir, exist_ok=True)
    os.makedirs(os.path.join(args.output_dir, "src"), exist_ok=True)
    csv_to_wav(args)


if __name__ == '__main__':
  main()
